import { AppRegistry } from 'react-native';
import TroveApp from './src/App';

AppRegistry.registerComponent('trove', () => TroveApp);
