import React, { Component } from 'react';
import { View, ScrollView, Text, Image, Dimensions, ActivityIndicator } from 'react-native';

import { connect } from 'react-redux';
import { Button } from 'react-native-elements';
import firebase from 'firebase';

import { BasicInput } from '../../../Components';
import { validateEmail } from '../../../Helpers';
import { registerUser } from '../../../Redux/Actions/User';

const { width, height } = Dimensions.get('window');

class Login extends Component {
    state = {}

    static navigationOptions = ({ navigation, screenProps }) => ({
        title: 'Login'
    });


    componentDidUpdate(prevProps, prevState) {
        console.log(this.props);
    }

    renderLoginComponent = () => {
        return (<View style={{ flex: 1, minWidth: width, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ width: '70%' }}>
                <Image style={{
                    alignSelf: 'center',
                    width: '80%'
                }} resizeMode={'contain'} source={require('../../../Assets/Images/logo.png')} />
                <View>
                    <BasicInput placeholderText={'Username'}></BasicInput>
                    <BasicInput secureTextEntry placeholderText={'Password'}></BasicInput>
                    <Button title={'Login'} backgroundColor={'#6600dd'} style={{ margin: 0, padding: 0 }}></Button>
                    <Button title={'Sign Up'} style={{ margin: 0, padding: 0, marginTop: 20 }}></Button>
                </View>
            </View>
        </View>);
    }

    toggleActivityIndicator = (bool) => {
        this.setState({
            showActivityIndicator: bool
        });
    }

    handleRegistration = (email, password, passwordConfirmation) => {
        this.props.toFeedScreen('This is variable a');
        console.log(email, password, passwordConfirmation);
        // const isEmailValid = validateEmail(email);
        // if (email === '' || email === undefined || email === null || isEmailValid === false) {
        //     console.log("Failed!");
        //     // Throw error toast message here or something
        // } else {
        //     // this.props.registerUser(email, password);
        // }
    }

    renderSignUpComponent = () => {
        return (<View style={{ flex: 1, minWidth: width, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ width: '70%' }}>
                 <Image style={{
                    alignSelf: 'center',
                    width: '80%'
                }} resizeMode={'contain'} source={require('../../../Assets/Images/logo.png')} /> 
                <View>
                    <BasicInput
                        autoCapitalize={'none'}
                        value={'flynn.darragh@gmail.com'}
                        inputValue={
                            (email) => {
                                this.setState({
                                    registration: { ...this.state.registration, email }
                                });
                            }
                        } placeholderText={'Email'} />
                    <BasicInput
                        value={'password'}
                        secureTextEntry inputValue={
                            (password) => {
                                this.setState({
                                    registration: { ...this.state.registration, password }
                                });
                            }
                        } placeholderText={'Password'} />
                    <BasicInput
                        value={'password'}
                        secureTextEntry inputValue={
                            (passwordConfirmation) => {
                                this.setState({
                                    registration: { ...this.state.registration, passwordConfirmation }
                                });
                            }
                        } placeholderText={'Password Confirmation'} />
                    <Button onPress={() => {
                        const { email, password, passwordConfirmation } = this.state.registration;
                        {/* this.handleRegistration(email, password, passwordConfirmation); */ }
                        this.props.mainFlow();
                    }} title={'Sign Up'} style={{ margin: 0, padding: 0, marginTop: 20 }} />
                </View>
            </View>
        </View >);
    }

    render() {
        return (
            <View>
                <ScrollView
                    pagingEnabled
                    horizontal
                    style={{ flexDirection: 'row' }}>
                    {this.renderSignUpComponent()}
                </ScrollView>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    mainFlow: () => dispatch({ type: 'Main' })
});

export default connect(null, mapDispatchToProps)(Login);