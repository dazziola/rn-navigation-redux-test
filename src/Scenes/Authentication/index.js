import LoginScene from './Login';
import RegistrationScene from './Registration';

export const Login = LoginScene;
export const Registration = RegistrationScene;