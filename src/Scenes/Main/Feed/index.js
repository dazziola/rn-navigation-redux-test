import React, { Component } from 'react';
import { ScrollView, View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/FontAwesome';
import { primaryColor } from '../../../Config/constants';
import ProductCard from '../../../Components/Cards/ProductCard';
import ItemSearch from '../../../Components/ItemSearch';

class Feed extends Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        title: 'Feed',
        header: null
    });

    state = {}
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff', justifyContent: 'center' }}>
                <ItemSearch />
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    goToItemDetail: () => dispatch({ type: 'ItemDetail' })
});

export default connect(null, mapDispatchToProps)(Feed);