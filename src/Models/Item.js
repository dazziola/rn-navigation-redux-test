export const User = {
  id: String,
  itemTitle: String,
  description: String,
  age: Date,
  itemPhotos: Array
}