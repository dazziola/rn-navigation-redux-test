export const User = {
  id: String,
  firstName: String,
  lastName: String,
  dateOfBirth: Date,
  profilePhoto: String,
  email: String,
  telephoneNumber: String,
  isEmailVerified: Boolean,
  isTelephoneVerified: Boolean,
  isIdentificationVerified: Boolean,
  paymentReferences: Object,
  identificationReferences: Object,
  address: Object({
    addressLine1: String,
    addressLine2: String,
    town: String,
    county: String,
    country: String
  })
}