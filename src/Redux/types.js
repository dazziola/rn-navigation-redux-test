// User Types
export const SET_CURRENT_USER = 'set_current_user';

// Item Types
export const UPDATE_CURRENT_SEARCH_TERM = 'update_current_search_term';