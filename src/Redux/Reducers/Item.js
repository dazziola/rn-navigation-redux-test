import { UPDATE_CURRENT_SEARCH_TERM } from '../types';

const INITIAL_STATE = {
  currentSearchTerm: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_CURRENT_SEARCH_TERM:
      return {
        ...state,
        currentSearchTerm: action.payload
      };
    default:
      return state;
  }
};
