import { combineReducers } from 'redux';
import NavReducer from '../Reducers/Nav';
import ItemReducer from '../Reducers/Item';

export default combineReducers({
    nav: NavReducer,
    item: ItemReducer
});
