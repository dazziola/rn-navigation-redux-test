import { NavigationActions } from 'react-navigation';
import { CombinedNavigation } from '../../Navigator';

const INITIAL_STATE = {
  // index: 1,
  // routes: [
  //   { key: 'InitA', routeName: 'Auth' },
  //   { key: 'InitB', routeName: 'Main' }
  // ]
};

console.log(CombinedNavigation);

// Start with two routes: The Main screen, with the Login screen on top.
// const firstAction = CombinedNavigation.router.getActionForPathAndParams('Auth');
// const tempNavState = CombinedNavigation.router.getStateForAction(firstAction);

const initialNavState = CombinedNavigation.router.getStateForAction(
  'Auth',
  CombinedNavigation.router.getStateForAction('Auth')
);

export default (state = initialNavState, action) => {
  switch (action.type) {
    case 'Main':
      nextState = CombinedNavigation.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Main' }),
        state
      );
      console.log(state);
      break;
    case 'ItemDetail':
      nextState = CombinedNavigation.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Item' }),
        state
      );
      console.log(state);
      break;
    default:
      nextState = CombinedNavigation.router.getStateForAction(action, state);
      console.log(state);
      break;
  }

  return nextState || state;

}