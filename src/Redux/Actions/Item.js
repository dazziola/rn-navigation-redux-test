import firebase from 'firebase';

import {
  UPDATE_CURRENT_SEARCH_TERM
} from '../types';

export const updateCurrentSearchTerm = (searchTerm) => {
  return (dispatch) => {
    dispatch({ type: UPDATE_CURRENT_SEARCH_TERM, payload: searchTerm })
  };
};
