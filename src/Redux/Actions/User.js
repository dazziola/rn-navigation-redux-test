import firebase from 'firebase';

import {
    SET_CURRENT_USER
} from '../types';

export const registerUser = (email, password) => {

    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
    });

    var user = firebase.auth().currentUser;
    if (user) {
        var name, email, photoUrl, uid, emailVerified;
        if (user != null) {
            name = user.displayName;
            email = user.email;
            photoUrl = user.photoURL;
            emailVerified = user.emailVerified;
            uid = user.uid;
            // return (dispatch) => {
            //     dispatch({ type: 'Login', payload: { name, email, photoUrl, emailVerified, uid } })
            // };
        }
    } 
    return () => null

};
