import React from 'react';
import { StackNavigator, TabNavigator, addNavigationHelpers } from 'react-navigation';
import { connect } from 'react-redux';
import { View } from 'react-native';

// Auth Navigation
import { Login, Registration } from './Scenes/Authentication';

// Feed Navigation
import Feed from './Scenes/Main/Feed';
import ItemDetail from './Scenes/Main/Feed/ItemDetail';

// Profile navigation
import Profile from './Scenes/Main/User';

// Chest navigation
import Chest from './Scenes/Main/Chest';


import Icon from 'react-native-vector-icons/FontAwesome';
import { primaryColor } from './Config/constants';

// Start of Navigator to handle Authentication and Registration

export const AuthenticationNavigation = StackNavigator(
    {
        Login: {
            screen: Login
        },
        Registration: {
            screen: Registration
        },
    },
    {
        headerMode: 'none',
        cardStyle: {
            backgroundColor: primaryColor,
            flex: 1,
            justifyContent: 'center'
        }
    }
);

export const FeedStack = StackNavigator(
    {
        Main: {
            screen: Feed,
            screenProps: {
                title: 'Feed'
            }
        },
        Item: {
            screen: ItemDetail,
            screenProps: {
                title: 'Item Detail'
            },
            navigationOptions: {
                
            }
        }
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: primaryColor
            },
            headerBackTitleStyle: {
                color: '#ffffff',
                fontWeight: 'bold'
            },
            tintColor: primaryColor,
            tabBarIcon: ({ tintColor }) => (
                <Icon
                    name={'search'}
                    style={[iconStyle, { color: tintColor }]}
                />
            )
        }
    }
);

export const ChestStack = StackNavigator(
    {
        Main: {
            screen: Chest,
            navigationOptions: {
                title: 'Chest'
            }
        }
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: primaryColor,
            },
            headerTitleStyle: {
                color: '#ffffff'
            },
            tabBarIcon: ({ tintColor }) => (
                <Icon
                    name={'diamond'}
                    style={[iconStyle, { color: tintColor }]}
                />
            )
        }
    }
)

export const UserStack = StackNavigator(
    {
        Profile: {
            screen: Profile,
            navigationOptions: {
                title: 'Profile'
            }
        }
    }, {
        navigationOptions: {
            headerTitleStyle: {
                color: '#ffffff'
            },
            headerStyle: {
                backgroundColor: primaryColor
            },
            tabBarIcon: ({ tintColor }) => (
                <Icon
                    name={'user'}
                    style={[iconStyle, { color: tintColor }]}
                />
            )
        }
    }
)

export const MainNavigation = TabNavigator(
    {
        Feed: {
            screen: FeedStack,
        },
        Chest: {
            screen: ChestStack,
        },
        User: {
            screen: UserStack
        }
    },
    {
        initialRouteName: 'Feed',
        tabBarOptions: {
            activeTintColor: primaryColor,
            inactiveTintColor: '#a6a6a6',
        },
    }
);


export const CombinedNavigation = StackNavigator(
    {
        Auth: {
            // screen: ({ dispatch, nav }) => (<AuthenticationNavigation navigation={
            //     addNavigationHelpers({ dispatch, state: nav })
            // } />)
            screen: AuthenticationNavigation
        },
        Main: {
            // screen: ({ dispatch, nav }) => (<MainNavigation navigation={
            //     addNavigationHelpers({ dispatch, state: nav })
            // } />)
            screen: MainNavigation
        },
    },
    {
        initialRouteName: 'Main',
        headerMode: 'none',
        cardStyle: {
            backgroundColor: 'gray',
            flex: 1,
            justifyContent: 'center'
        }
    }
);

const iconStyle = {
    fontSize: 20
};

const AppWithNavigationState = ({ dispatch, nav }) => (
    <CombinedNavigation navigation={
        addNavigationHelpers({ dispatch, state: nav })
    } />
);

const mapStateToProps = (state) => ({
    nav: state.nav
});

export default connect(mapStateToProps)(AppWithNavigationState);