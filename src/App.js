import React, { Component } from 'react';

import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';

import AppWithNavigationState from './Navigator';
import reducers from './Redux/Reducers';
import firebaseConfig from './Config/firebase';

class TroveApp extends Component {

    componentWillMount() {
        firebase.initializeApp(firebaseConfig);
    }

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <Provider store={store}>
                <AppWithNavigationState />
            </Provider >
        );
    }
}

export default TroveApp;
