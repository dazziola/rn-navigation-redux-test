import React, { Component } from 'react';
import { View, ScrollView, Text, TextInput, Dimensions, Animated, Easing, TouchableHighlight, LayoutAnimation } from 'react-native';
import { primaryColor } from '../../Config/constants';
import { connect } from 'react-redux';
import { updateCurrentSearchTerm } from '../../Redux/Actions/Item';
import Icon from 'react-native-vector-icons/FontAwesome';
import ProductCard from '../Cards/ProductCard';

const { width, height } = Dimensions.get('window');

class ItemSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      animatedValue: new Animated.Value(0),
      isInputOpen: false,
      defaultHeaderHeight: 64,
      inputIsAcceptingText: false,
      inputValue: null
    }
  }

  componentDidMount() {
    Animated.spring(this.state.animatedValue, {
      duration: 10000,
      toValue: 1
    }).start()
  }

  componentDidUpdate(prevProps, prevState) {
    LayoutAnimation.spring();
  }

  render() {
    return (
      <Animated.View style={[{
        paddingTop: 10,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        transform: [{
          scaleX: this.state.animatedValue
        },
        {
          scaleY: this.state.animatedValue
        }],
        height: this.state.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [this.state.defaultHeaderHeight, height - 49]
        }),
        flexDirection: 'column'
      }, this.state.inputIsAcceptingText ? { justifyContent: 'flex-start' } : { justifyContent: 'center' }]}>

        <View style={[{ flex: 1 }, this.state.inputIsAcceptingText ? { justifyContent: 'flex-start', paddingTop: 16 } : { justifyContent: 'center' }]}>
          <TextInput
            style={{
              borderColor: 'blue',
              borderWidth: 2,
              width: "100%",
              borderBottomColor: primaryColor,
              paddingBottom: 10,
              borderBottomWidth: 3,
              fontSize: 32,
              color: primaryColor,
              marginHorizontal: 20,
            }}
            onChangeText={text => {
              if (text.length > 0) {
                this.setState({
                  inputIsAcceptingText: true,
                  inputValue: text
                })
              } else {
                this.setState({
                  inputIsAcceptingText: false,
                  text: null
                })
              }
              this.props.updateCurrentSearchTerm(text);
            }}
            placeholder={'Search for items here...'}
            placeholderTextColor={primaryColor}
          />
        </View>

      </Animated.View >
    );
  }
}

const mapStateToProps = state => {
  const { currentSearchTerm } = state.item;
  return { currentSearchTerm };
}

export default connect(mapStateToProps, { updateCurrentSearchTerm })(ItemSearch);