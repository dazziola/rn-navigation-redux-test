import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native';

class BasicInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: this.props.value,
            placeholderText: this.props.placeholderText || 'Text here'
        }
    }

    componentDidMount() {
        // this.props.inputValue(this.props.value);
        if (this.props.secureTextEntry) {
            this._textInput.setNativeProps({ secureTextEntry: true });
        }
        if (this.props.autoCapitalize && Platform.OS === 'ios') {
            this._textInput.setNativeProps({ autoCapitalize: this.props.autoCapitalize });
        }
    }

    render() {
        const {
            height,
            width,
            backgroundColor,
            paddingHorizontal,
            margin,
            borderBottomColor,
            borderBottomWidth,
            color,
            marginBottom,
            placeholderTextColor
        } = this.props;
        return (
            <View style={[style.inputContainer, {
                height,
                width,
                paddingHorizontal,
                borderBottomColor,
                borderBottomWidth,
                marginBottom
            }]}>
                <TextInput
                    onLayout={() => {
                        if (this.props.inputValue) {
                            this.props.inputValue(this.props.value);
                        }
                    }}
                    ref={component => this._textInput = component}
                    onChangeText={inputValue => {
                        this.setState({
                            inputValue
                        });
                        if (this.props.inputValue) {
                            this.props.inputValue(inputValue);
                        }
                    }}
                    value={this.state.inputValue}
                    style={[style.input, { color }]}
                    placeholderTextColor={placeholderTextColor}
                    placeholder={this.state.placeholderText}
                    selectionColor={'white'}
                />
            </View >
        );
    }
}

const style = StyleSheet.create({
    input: {
        flex: 1
    }
});

BasicInput.defaultProps = {
    height: 50,
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.01)',
    borderBottomColor: '#ffffff',
    borderBottomWidth: 2,
    paddingHorizontal: 5,
    margin: 0,
    color: '#ffffff',
    placeholderTextColor: 'rgba(255,255,255,0.4)',
    marginBottom: 10
};

export default BasicInput;