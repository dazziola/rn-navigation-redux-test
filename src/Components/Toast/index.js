import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Toast extends Component {
    state = {}
    render() {
        return (
            <View style={{
                position: 'absolute',
                zIndex: 9999,
                bottom: 0,
                width: '100%',
                borderColor: 'red',
                borderWidth: 1
            }}>
                <Text>Toast here</Text>
            </View>
        );
    }
}

export default Toast;