import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { User } from '../../../Models/User';
class ProductCard extends Component {
  
  state = {};

  handlePress = () => {
    const user1 = User;
    console.log(user1);
    this.props.onPressCallback();
  }

  render() {
    return (
      <TouchableOpacity style={styles.card} onPress={
        () => {
          this.handlePress();
        }
      }>
        <Text style={styles.text}>Product Title</Text>
        <Text style={styles.text}>Availability</Text>
        <Text style={styles.text}>Daily Rate</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#ffffff',
    marginHorizontal: 10,
    marginVertical: 5
  },
  text: {
    padding: 10
  }
});

export default ProductCard;